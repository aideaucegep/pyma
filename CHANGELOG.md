#Version 1.4
This version doesn't change anything big.

* Fixed up some information on the readme about 1.3 changes
* Cleaned up repo

#Version 1.3
* Added Cumulative Moving Average (CMA) and Weighted Moving Average (WMA).
* Added a CHANGELOG.md file.